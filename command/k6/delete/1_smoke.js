import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'stock_command.proto');

export const options = {
    ext: {
        loadimpact: {
            projectID: 3603316,
            name: "delete_smoke"
        }
    },
    vus: 50,
    duration: '1m',
    thresholds: {
        grpc_req_duration: [{threshold: 'p(99)<1500'}],
        iteration_duration: [{threshold: 'p(99)<1500'}],
    },
};

export default function () {

    client.connect('host.docker.internal:9003', {
        plaintext: true,
        reflect: true,
        timeout: "10s"
    });

    const data = {
        uuid: "6357ed8fe57d53d948fd9550",
    };
    const responseUpdate = client.invoke('stock_command.StockCommand/Delete', data);

    check(responseUpdate, {
        'delete is ok': (r) => r && r.status === grpc.StatusOK,
    });

    client.close();
    sleep(1);
};
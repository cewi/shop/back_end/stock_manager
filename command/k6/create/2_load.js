import grpc from 'k6/net/grpc';
import {check} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'stock_command.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "create_load"
    }
  },
  stages: [
    {duration: '2m', target: 50},
    {duration: '5m', target: 50},
    {duration: '2m', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(95)<2000'},{threshold: 'max<30000'}],
    iteration_duration: [{threshold: 'p(95)<2000'}],
  },
};

export default function () {

  client.connect('host.docker.internal:9003', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });

  const data = {
    physicalQuantity: 10,
    virtualQuantity: 15,
    productId: "ec67570d-8047-4a19-be98-496dac95c81c",
    damagedQuantity: 1,
  };
  const res = client.invoke('stock_command.StockCommand/Create', data);

  check(res, {
    'create success ok': (r) => {
      return r && r.status === grpc.StatusOK
    },
    'mensage response ok': (r) => {
      return r && r.message.message === "estoque, criado"
    },
  });

  if (!res) {
    fail('unexpected response');
  }

  client.close();
};
import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'stock_command.proto');

export const options = {
    ext: {
        loadimpact: {
            projectID: 3603316,
            name: "update_stress_2"
        }
    },
    stages: [
        {duration: '10s', target: 40},
        {duration: '1m', target: 40},
        {duration: '10s', target: 50},
        {duration: '3m', target: 50},
        {duration: '10s', target: 40},
        {duration: '3m', target: 40},
        {duration: '10s', target: 0},
    ],
    thresholds: {
        grpc_req_duration: [{threshold: 'p(100)<3000'}],
        iteration_duration: [{threshold: 'p(100)<10000'}],
    },
};

export default function () {

    client.connect('host.docker.internal:9003', {
        plaintext: true,
        reflect: true,
        timeout: "10s"
    });

    const data = {
        uuid: "63387434a64d61e0f97004c5",
        physicalQuantity: 10,
        virtualQuantity: 15,
        productId: "ec67570d-8047-4a19-be98-496dac95c81c",
        damagedQuantity: 1,
        create_at: "2022-09-27T15:07:56.572316900Z"
    };

    const responseUpdate1 = client.invoke('stock_command.StockCommand/Update', data);
    const responseUpdate2 = client.invoke('stock_command.StockCommand/Update', data);
    const responseUpdate3 = client.invoke('stock_command.StockCommand/Update', data);
    const responseUpdate4 = client.invoke('stock_command.StockCommand/Update', data);

    check(responseUpdate1, {
        'create is ok 1': (r) => r && r.status === grpc.StatusOK,
    });
    check(responseUpdate2, {
        'create is ok 1': (r) => r && r.status === grpc.StatusOK,
    });
    check(responseUpdate3, {
        'create is ok 1': (r) => r && r.status === grpc.StatusOK,
    });
    check(responseUpdate4, {
        'create is ok 1': (r) => r && r.status === grpc.StatusOK,
    });

    client.close();
    sleep(1);
};
import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'stock_command.proto');

export const options = {
    ext: {
        loadimpact: {
            projectID: 3603316,
            name: "update_load"
        }
    },
    stages: [
        {duration: '2m', target: 50},
        {duration: '5m', target: 50},
        {duration: '2m', target: 0},
    ],
    thresholds: {
        grpc_req_duration: [{threshold: 'p(99)<1500'}],
        iteration_duration: [{threshold: 'p(99)<1500'}],
    },
};

export default function () {

    client.connect('host.docker.internal:9003', {
        plaintext: true,
        reflect: true,
        timeout: "10s"
    });

    const data = {
        uuid: "63387434a64d61e0f97004c5",
        physicalQuantity: 10,
        virtualQuantity: 15,
        productId: "ec67570d-8047-4a19-be98-496dac95c81c",
        damagedQuantity: 1,
        create_at: "2022-09-27T15:07:56.572316900Z"
    };
    const responseUpdate = client.invoke('stock_command.StockCommand/Update', data);

    check(responseUpdate, {
        'update is ok': (r) => r && r.status === grpc.StatusOK,
    });

    client.close();
};
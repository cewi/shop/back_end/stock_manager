package main

import (
  application "gitlab.com/cewi/shop/back_end/stock_manager/command/src"
  "log"
)

func main() {
  log.Println("Inicializando serviço Stock Command")
  application.NewApplication().Init()
}

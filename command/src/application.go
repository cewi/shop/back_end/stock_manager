package application

import (
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/controller"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/proto_buffer"
  "google.golang.org/grpc"
  "google.golang.org/grpc/reflection"
  "log"
  "net"
  "os"
)

type application struct {
  stockCommandServer proto_buffer.StockCommandServer
}

const (
  TCP = "tcp"
)

func (a *application) Init() {

  grpcServer := grpc.NewServer()
  proto_buffer.RegisterStockCommandServer(grpcServer, a.stockCommandServer)
  reflection.Register(grpcServer)

  port := os.Getenv("GRPC_PORT")
  listener, err := net.Listen(TCP, ":"+port)
  if err != nil {
    log.Fatalf(err.Error())
  }
  log.Printf("Applicação Stock Command, inicializada com sucesso na porta: %v\n", port)
  errs := grpcServer.Serve(listener)
  if errs != nil {
    log.Fatalf(err.Error())
  }
}

func NewApplication() *application {
  stockController := framework_controller.NewStockController()
  return &application{
    stockCommandServer: stockController,
  }
}

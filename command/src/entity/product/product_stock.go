package entity_product

import (
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/entity/stock"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework"
)

type IProduct interface {
  Stock() *entity_stock.IStock
  ProductId() string
}

type Product struct {
  productId string
  stock     *entity_stock.IStock
}

func (p Product) ProductId() string {
  return p.productId
}

func (p Product) Stock() *entity_stock.IStock {
  return p.stock
}

func (p Product) IsValid(product Product) *framework.Error {
  if product.productId == "" {
    newError := framework.NewError("produto não encontrado", 1001)
    return &newError
  }
  return nil
}

func NewProductStock(productId string, stock entity_stock.IStock) (IProduct, *framework.Error) {
  product := Product{
    stock:     &stock,
    productId: productId,
  }
  err := product.IsValid(product)
  if err != nil {
    return nil, err
  }
  return product, nil
}

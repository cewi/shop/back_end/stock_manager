package entity_stock

import "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework"

type IStock interface {
  VirtualQuantity() int64
  DamagedQuantity() int64
  IsValid() *framework.Error
  PhysicalQuantity() int64
}

type stock struct {
  physicalQuantity int64
  virtualQuantity  int64
  damagedQuantity  int64
}

func NewStock(physicalQuantity int64, virtualQuantity int64, damagedQuantity int64) (IStock, *framework.Error) {
  stock := stock{
    physicalQuantity: physicalQuantity,
    virtualQuantity:  virtualQuantity,
    damagedQuantity:  damagedQuantity,
  }
  err := stock.IsValid()
  if err != nil {
    return nil, err
  }
  return stock, nil
}

func (s stock) PhysicalQuantity() int64 {
  return s.physicalQuantity
}

func (s stock) VirtualQuantity() int64 {
  return s.virtualQuantity
}

func (s stock) DamagedQuantity() int64 {
  return s.damagedQuantity
}

func (s stock) IsValid() *framework.Error {

  if s.virtualQuantity < int64(0) {
    newError := framework.NewError("estoque virtual não preenchido", 1002)
    return &newError
  }

  if s.physicalQuantity < int64(0) {
    newError := framework.NewError("estoque físico não preenchido", 1003)
    return &newError
  }

  if s.damagedQuantity < int64(0) {
    newError := framework.NewError("estoque danificado não pode ser um número negativo", 1004)
    return &newError
  }
  return nil
}

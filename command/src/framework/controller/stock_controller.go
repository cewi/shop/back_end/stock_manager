package framework_controller

import (
  "context"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/proto_buffer"
  repository_database "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/database"
  repository_stock "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/stock"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/use_case"
)

type stockController struct {
  stockInteractor use_case.IStockInteractor
}

func (s stockController) Create(_ context.Context, createRequest *proto_buffer.CreateStockRequest) (*proto_buffer.CreateStockResponse, error) {
  return s.stockInteractor.Create(createRequest)
}
func (s stockController) Update(_ context.Context, updateRequest *proto_buffer.UpdateStockRequest) (*proto_buffer.UpdateStockResponse, error) {
  return s.stockInteractor.Update(updateRequest)
}
func (s stockController) Delete(_ context.Context, deleteRequest *proto_buffer.DeleteStockRequest) (*proto_buffer.DeleteStockResponse, error) {
  return s.stockInteractor.Delete(deleteRequest)
}

func NewStockController() proto_buffer.StockCommandServer {
  mongoDataSource := repository_database.NewMongoDataSource()
  stockRepository := repository_stock.NewStockRepository(mongoDataSource)
  stockInteractor := use_case.NewStockInteractor(stockRepository)
  return stockController{
    stockInteractor: stockInteractor,
  }
}

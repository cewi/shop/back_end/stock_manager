package repository_stock

import (
  "context"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/database"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type GenericRepository struct {
  MongoDataSource repository_database.IMongoDataSource
  DatabaseName    string
  TableName       string
}

func (c GenericRepository) Save(mapper interface{}) (string, error) {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()
  response, err := collection.InsertOne(ctx, mapper)
  if err != nil {
    return "", err
  }
  hex := response.InsertedID.(primitive.ObjectID).Hex()
  return hex, nil
}

func (c GenericRepository) Update(id primitive.ObjectID, mapper interface{}) error {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()
  filter := bson.M{"_id": bson.M{"$eq": id}}
  _, err := collection.UpdateOne(ctx, filter, bson.M{"$set": mapper})
  if err != nil {
    return err
  }
  return nil

}

func (c GenericRepository) Delete(id primitive.ObjectID) error {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()
  filter := bson.M{"_id": id}
  _, err := collection.DeleteOne(ctx, filter)
  if err != nil {
    return err
  }
  return nil

}

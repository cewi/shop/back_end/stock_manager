package repository_stock

import (
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework"
  repositorydatabase "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/database"
  repositorymapper "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/mapper"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

const (
  databaseName = "stock_manager"
  tableName    = "stock"
)

type IStockRepository interface {
  Save(mapper repositorymapper.CreateStockMapper) (*string, *framework.Error)
  Update(mapper repositorymapper.UpdateStockMapper) *framework.Error
  Delete(uuid string) *framework.Error
}

type stockRepository struct {
  GenericRepository
}

func (c stockRepository) Save(mapper repositorymapper.CreateStockMapper) (*string, *framework.Error) {
  save, err := c.GenericRepository.Save(mapper)
  if err != nil {
    errRepo := framework.NewError(err.Error(), int64(2001))
    return nil, &errRepo
  }
  return &save, nil
}

func (c stockRepository) Update(mapper repositorymapper.UpdateStockMapper) *framework.Error {
  err := c.GenericRepository.Update(mapper.Uuid, mapper)
  if err != nil {
    newError := framework.NewError(err.Error(), int64(2002))
    return &newError
  }
  return nil
}

func (c stockRepository) Delete(uuid string) *framework.Error {
  hex, _ := primitive.ObjectIDFromHex(uuid)
  err := c.GenericRepository.Delete(hex)
  if err != nil {
    newError := framework.NewError(err.Error(), int64(2003))
    return &newError
  }
  return nil
}

func NewStockRepository(mongoDataSource repositorydatabase.IMongoDataSource) IStockRepository {
  return stockRepository{
    GenericRepository{
      MongoDataSource: mongoDataSource,
      DatabaseName:    databaseName,
      TableName:       tableName,
    },
  }
}

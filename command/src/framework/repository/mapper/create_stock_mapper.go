package repository_mapper

import "time"

type CreateStockMapper struct {
  PhysicalQuantity int64     `bson:"physical_quantity"`
  VirtualQuantity  int64     `bson:"virtual_quantity"`
  ProductId        string    `bson:"product_id"`
  CreateAt         time.Time `bson:"create_at"`
  DamagedQuantity  int64     `bson:"damaged_quantity"`
}

package repository_mapper

type CreateProductStockMapper struct {
  ProductId string `bson:"product_id"`
  Stock     CreateStockMapper
}

package repository_mapper

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type UpdateStockMapper struct {
  Uuid             primitive.ObjectID `bson:"_id"`
  PhysicalQuantity int64              `bson:"physical_quantity"`
  VirtualQuantity  int64              `bson:"virtual_quantity"`
  ProductId        string             `bson:"product_id"`
  UpdateAt         time.Time          `bson:"update_at"`
  DamagedQuantity  int64              `bson:"damaged_quantity"`
}

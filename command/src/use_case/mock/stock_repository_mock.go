package mock

import (
  "github.com/stretchr/testify/mock"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework"
  repositorymapper "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/mapper"
)

type StockRepositoryMock struct {
  mock.Mock
}

func (mock *StockRepositoryMock) Save(mapper repositorymapper.CreateStockMapper) (*string, *framework.Error) {
  args := mock.Called()
  result := args.Get(0)
  err := args.Error(1).(*framework.Error)
  return (result).(*string), err
}

func (mock *StockRepositoryMock) Update(mapper repositorymapper.UpdateStockMapper) *framework.Error {
  args := mock.Called()
  result := args.Get(0)
  return (result).(*framework.Error)
}

func (mock *StockRepositoryMock) Delete(uuid string) *framework.Error {
  args := mock.Called()
  result := args.Get(0)
  return (result).(*framework.Error)
}

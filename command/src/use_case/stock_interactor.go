package use_case

import (
  entity_product "gitlab.com/cewi/shop/back_end/stock_manager/command/src/entity/product"
  entity_stock "gitlab.com/cewi/shop/back_end/stock_manager/command/src/entity/stock"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/proto_buffer"
  repository_mapper "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/mapper"
  repository_stock "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/repository/stock"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type IStockInteractor interface {
  Create(request *proto_buffer.CreateStockRequest) (*proto_buffer.CreateStockResponse, error)
  Update(request *proto_buffer.UpdateStockRequest) (*proto_buffer.UpdateStockResponse, error)
  Delete(request *proto_buffer.DeleteStockRequest) (*proto_buffer.DeleteStockResponse, error)
}

type stockInteractor struct {
  stockRepository repository_stock.IStockRepository
}

func NewStockInteractor(stockRepository repository_stock.IStockRepository) IStockInteractor {
  return stockInteractor{stockRepository: stockRepository}
}

func (c stockInteractor) Create(inputBoundary *proto_buffer.CreateStockRequest) (*proto_buffer.CreateStockResponse, error) {
  productStock, err := c.toEntity(inputBoundary)
  if err != nil {
    return c.createFail(err), nil
  }

  var stock = *productStock.Stock()
  stockMapper := repository_mapper.CreateStockMapper{
    ProductId:        productStock.ProductId(),
    VirtualQuantity:  stock.VirtualQuantity(),
    PhysicalQuantity: stock.PhysicalQuantity(),
    DamagedQuantity:  stock.DamagedQuantity(),
    CreateAt:         time.Now(),
  }

  _, errRepo := c.stockRepository.Save(stockMapper)
  if errRepo != nil {
    return c.createFail(errRepo), nil
  }
  return &proto_buffer.CreateStockResponse{Message: "estoque, criado"}, nil
}
func (c stockInteractor) Update(inputBoundary *proto_buffer.UpdateStockRequest) (*proto_buffer.UpdateStockResponse, error) {
  productStock, err := c.toUpdateEntity(inputBoundary)
  if err != nil {
    return c.updateFail(err), nil
  }

  uuid, _ := primitive.ObjectIDFromHex(inputBoundary.Uuid)

  var stock = *productStock.Stock()
  stockMapper := repository_mapper.UpdateStockMapper{
    Uuid:             uuid,
    ProductId:        productStock.ProductId(),
    DamagedQuantity:  stock.DamagedQuantity(),
    VirtualQuantity:  stock.VirtualQuantity(),
    PhysicalQuantity: stock.PhysicalQuantity(),
    UpdateAt:         time.Now(),
  }

  errRepo := c.stockRepository.Update(stockMapper)
  if errRepo != nil {
    return c.updateFail(errRepo), nil
  }

  return &proto_buffer.UpdateStockResponse{Message: "estoque, alterado"}, nil
}
func (c stockInteractor) Delete(inpuBoundary *proto_buffer.DeleteStockRequest) (*proto_buffer.DeleteStockResponse, error) {
  errRepo := c.stockRepository.Delete(inpuBoundary.Uuid)
  if errRepo != nil {
    return c.deleteFail(errRepo), nil
  }
  return &proto_buffer.DeleteStockResponse{Message: "estoque, deletado"}, nil
}

func (c stockInteractor) createFail(err *framework.Error) *proto_buffer.CreateStockResponse {
  return &proto_buffer.CreateStockResponse{
    Error: &proto_buffer.Error{
      Code:  err.Code(),
      Error: err.Error(),
    },
  }
}

func (c stockInteractor) deleteFail(err *framework.Error) *proto_buffer.DeleteStockResponse {
  return &proto_buffer.DeleteStockResponse{
    Error: &proto_buffer.Error{
      Code:  err.Code(),
      Error: err.Error(),
    },
  }
}

func (c stockInteractor) updateFail(err *framework.Error) *proto_buffer.UpdateStockResponse {
  return &proto_buffer.UpdateStockResponse{
    Error: &proto_buffer.Error{
      Code:  err.Code(),
      Error: err.Error(),
    },
  }
}

func (c stockInteractor) toEntity(inputBoundary *proto_buffer.CreateStockRequest) (entity_product.IProduct, *framework.Error) {
  stock, errStock := entity_stock.NewStock(
    inputBoundary.PhysicalQuantity,
    inputBoundary.VirtualQuantity,
    inputBoundary.DamagedQuantity,
  )
  if errStock != nil {
    return nil, errStock
  }
  return entity_product.NewProductStock(inputBoundary.ProductId, stock)
}

func (c stockInteractor) toUpdateEntity(updateBoundary *proto_buffer.UpdateStockRequest) (entity_product.IProduct, *framework.Error) {
  stock, errStock := entity_stock.NewStock(
    updateBoundary.PhysicalQuantity,
    updateBoundary.VirtualQuantity,
    updateBoundary.DamagedQuantity,
  )
  if errStock != nil {
    return nil, errStock
  }
  return entity_product.NewProductStock(updateBoundary.ProductId, stock)
}

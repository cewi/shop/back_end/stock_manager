package use_case

import (
  "github.com/stretchr/testify/assert"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/framework/proto_buffer"
  "gitlab.com/cewi/shop/back_end/stock_manager/command/src/use_case/mock"
  "testing"
)

func Test_devera_criar_um_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Save").Return(&uuid, err)

  stockRequest := proto_buffer.CreateStockRequest{
    ProductId:        "276029c1-cefa-430f-b471-6010a123eb92",
    DamagedQuantity:  int64(100),
    VirtualQuantity:  int64(50),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Create(&stockRequest)

  assertions.NotNil(response)
  assertions.Equal("estoque, criado", response.Message)
  assertions.Nil(response.Error)

}

func Test_devera_validar_se_o_produto_foi_informado(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Save").Return(&uuid, err)

  stockRequest := proto_buffer.CreateStockRequest{
    DamagedQuantity:  int64(100),
    VirtualQuantity:  int64(50),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Create(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "produto não encontrado")
  assertions.Equal(response.Error.Code, int64(1001))

}

func Test_devera_validar_estoque_virtual_e_negativo(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Save").Return(&uuid, err)

  stockRequest := proto_buffer.CreateStockRequest{
    ProductId:        "276029c1-cefa-430f-b471-6010a123eb92",
    VirtualQuantity:  int64(-50),
    DamagedQuantity:  int64(100),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Create(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "estoque virtual não preenchido")
  assertions.Equal(response.Error.Code, int64(1002))

}

func Test_devera_validar_estoque_fisico_e_negativo(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Save").Return(&uuid, err)

  stockRequest := proto_buffer.CreateStockRequest{
    ProductId:        "276029c1-cefa-430f-b471-6010a123eb92",
    VirtualQuantity:  int64(50),
    DamagedQuantity:  int64(100),
    PhysicalQuantity: int64(-20),
  }

  response, _ := stockInteractor.Create(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "estoque físico não preenchido")
  assertions.Equal(response.Error.Code, int64(1003))

}

func Test_devera_validar_estoque_danificado_e_negativo(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Save").Return(&uuid, err)

  stockRequest := proto_buffer.CreateStockRequest{
    ProductId:        "276029c1-cefa-430f-b471-6010a123eb92",
    VirtualQuantity:  int64(50),
    DamagedQuantity:  int64(-100),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Create(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "estoque danificado não pode ser um número negativo")
  assertions.Equal(response.Error.Code, int64(1004))

}

func Test_devera_validar_erro_ocorrido_ao_criar_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  newError := framework.NewError("erro ocorrido ao tentar salvar o estoque", 2001)
  stockRepositoryMock.On("Save").Return(&uuid, &newError)

  stockRequest := proto_buffer.CreateStockRequest{
    ProductId:        "276029c1-cefa-430f-b471-6010a123eb92",
    VirtualQuantity:  int64(50),
    DamagedQuantity:  int64(100),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Create(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "erro ocorrido ao tentar salvar o estoque")
  assertions.Equal(response.Error.Code, int64(2001))

}

func Test_devera_alterar_um_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Update").Return(err)

  stockRequest := proto_buffer.UpdateStockRequest{
    Uuid:             uuid,
    ProductId:        "276029c1-cefa-430f-b471-6010a123eb92",
    DamagedQuantity:  int64(100),
    VirtualQuantity:  int64(50),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Update(&stockRequest)

  assertions.NotNil(response)
  assertions.Equal("estoque, alterado", response.Message)
  assertions.Nil(response.Error)
}

func Test_devera_validar_error_ao_alterar_um_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  newError := framework.NewError("erro ocorrido ao tentar salvar o estoque", 2001)
  stockRepositoryMock.On("Update").Return(&newError)

  stockRequest := proto_buffer.UpdateStockRequest{
    Uuid:             uuid,
    ProductId:        "91d1c7bf-23dc-4697-8f14-f5f7f6c8175t",
    DamagedQuantity:  int64(100),
    VirtualQuantity:  int64(50),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Update(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "erro ocorrido ao tentar salvar o estoque")
  assertions.Equal(response.Error.Code, int64(2001))
}

func Test_devera_validar_produto_informado_ao_alterar_um_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  newError := framework.NewError("erro ocorrido ao tentar salvar o estoque", 2001)
  stockRepositoryMock.On("Update").Return(&newError)

  stockRequest := proto_buffer.UpdateStockRequest{
    Uuid:             uuid,
    DamagedQuantity:  int64(100),
    VirtualQuantity:  int64(50),
    PhysicalQuantity: int64(20),
  }

  response, _ := stockInteractor.Update(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "produto não encontrado")
  assertions.Equal(response.Error.Code, int64(1001))
}

func Test_devera_deletar_um_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  var err *framework.Error
  stockRepositoryMock.On("Delete").Return(err)

  stockRequest := proto_buffer.DeleteStockRequest{Uuid: uuid}

  response, _ := stockInteractor.Delete(&stockRequest)

  assertions.NotNil(response)
  assertions.Equal("estoque, deletado", response.Message)
  assertions.Nil(response.Error)
}

func Test_devera_validar_erro_ao_tentar_deletar_um_estoque(t *testing.T) {
  assertions := assert.New(t)

  stockRepositoryMock := new(mock.StockRepositoryMock)
  stockInteractor := NewStockInteractor(stockRepositoryMock)

  uuid := "91d1c7bf-23dc-4697-8f14-f5f7f6c817eb"
  err := framework.NewError("erro ao tentar deletar estoque", 2003)
  stockRepositoryMock.On("Delete").Return(&err)

  stockRequest := proto_buffer.DeleteStockRequest{Uuid: uuid}

  response, _ := stockInteractor.Delete(&stockRequest)

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal(response.Error.Error, "erro ao tentar deletar estoque")
  assertions.Equal(response.Error.Code, int64(2003))
}
